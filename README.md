## Traffic Generator

This project is to generate request traffic on a GitLab environment


This will eventually cover three types of traffic patterns:

- https requests
- git https and git ssh
- gitlab-qa test cases

Currently it is only generating https requests

## HTTPS requests:

* To generate traffic we want to be able to exercise each of the backends, most of these requests can be exercised with simple https requests both authenticated and non-authenticated.
* The configuration is specified in [urls.yml](/urls.yml), different urls can be specified with rates specified individually for different urls.

For example:

```
https:
  - url: https://staging.gitlab.com/users/sign_in
    rate: 10
  - url: https://staging.gitlab.com/gitlab-com/infrastructure/issues/57
  - url: https://staging.gitlab.com/gitlab-com/operations/issues/42
  - url : https://staging.gitlab.com/dashboard/todos?state=done
    auth: yes
```

will generate requests sent to four urls, the first one at a rate of 10 requests per second and the others at the default rate of 2 per second.
the fourth url will use authentication.
