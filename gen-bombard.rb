#!/usr/bin/env ruby
require 'yaml'
require 'optparse'

options = {}
OptionParser.new do |opts|
  opts.banner = 'Usage: gen-bombard.rb [options]'

  opts.on('-b', '--backend BACKEND', 'backend of backend [api|https_git|https|registry|pages]') do |backend|
    options[:backend] = backend
  end

  opts.on('-t', '--token TOKEN', 'personal access token for authenticated requests') do |token|
    options[:token] = token
  end
end.parse!

urls = YAML.load_file(File.join(File.dirname(File.expand_path(__FILE__)), 'urls.yml'))

DEFAULT_OPTS = {
  'connections' => '20',
  'timeout' => '5s',
  'rate' => '2'
}
DEFAULT_DURATION = '10s'

urls[options[:backend]].each do |url|
  cmd = []
  DEFAULT_OPTS.keys.each do |opt|
    cmd << "--#{opt}=#{url[opt] || DEFAULT_OPTS[opt]}"
  end
  cmd << "--header='Private-Token: #{options[:token]}'" if url['auth']
  cmd << '--'
  cmd << url['url']
  puts cmd.join(' ')
end
