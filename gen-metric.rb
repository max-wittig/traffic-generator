#!/usr/bin/env ruby
require 'json'

def parse_results(object, tmp = [])
  case object
  when Array
    object.each.with_index(1).with_object({}) do |(element, i), result|
      result.merge! parse_results(element, tmp + [i])
    end
  when Hash
    object.each_with_object({}) do |(key, value), result|
      result.merge! parse_results(value, tmp + [key])
    end
  else
    { tmp.join('_') => object }
  end
end

json_input = $stdin
spec_labels = {}
spec_labels['auth'] = 'false'
test_results = JSON.parse(json_input.read)
test_results['spec'].each do |k, v|
  if k == 'headers'
    spec_labels['auth'] = 'true'
    next
  end
  spec_labels[k] = v
end
results = parse_results(test_results['result'])
results['errors_count'] = 0 unless results.key?('errors_count')
results['errors_count'] = results.delete('errors_1_count') if results.key?('errors_1_count')

joined_labels = spec_labels.map { |k, v| "#{k}=\"#{v}\"" }.join(',')
results.each do |k, v|
  next if v.is_a?(String)

  if k == 'others' || k.start_with?('req')
    k = k.gsub(/^req/, '')
    k = "num_status_#{k}"
  end
  if k.include?('latency')
    # convert from microseconds to milliseconds
    v /= 1000
  end
  puts "load_gen_result{#{joined_labels},result=\"#{k}\"} #{v.round(2)}"
end
